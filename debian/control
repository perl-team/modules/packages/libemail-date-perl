Source: libemail-date-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Rules-Requires-Root: no
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libcapture-tiny-perl,
                     libemail-abstract-perl,
                     libemail-date-format-perl,
                     libtimedate-perl,
                     perl,
                     perl | libtest-simple-perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libemail-date-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libemail-date-perl.git
Homepage: https://metacpan.org/release/Email-Date

Package: libemail-date-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libemail-abstract-perl,
         libemail-date-format-perl,
         libtimedate-perl
Description: Perl module for correct formatting of dates in emails
 RFC 2822 defines the Date: header. It declares the header a required
 part of an email message. The syntax for date headers is clearly laid
 out. Still, even a perfectly planned world has storms. The truth is, many
 programs get it wrong. Very wrong. Or, they don't include a Date: header
 at all. This often forces you to look elsewhere for the date, and hoping
 to find something.
 .
 For this reason, the tedious process of looking for a valid date has been
 encapsulated in this software. Further, the process of creating RFC
 compliant date strings is also found in this software.
